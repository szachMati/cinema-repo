
--cinema
INSERT INTO cinema(name, close_hour, email, manager, open_hour, phone, address_id)
    VALUES ('Multikino Wola Park',
            '00:00',
            'wolapark@gmail.com',
            'Jan Kowalski',
             '00:00', '600120120', (SELECT id from cinema_address_dict
                                        where city='Warszawa' and zip_code = '00-280'));

INSERT INTO cinema(name, close_hour, email, manager, open_hour, phone, address_id)
VALUES ('Multikino Złote Tarasy',
        '00:00',
        'zlot@tarasy.pl',
        'Marian Kowalski',
        '09:00', '670700890', (SELECT id from cinema_address_dict
                                     where city='Warszawa' and zip_code = '01-110'));

INSERT INTO cinema(name, close_hour, email, manager, open_hour, phone, address_id)
VALUES ('Multikino Krucza Warszawa',
        '22:30',
        'krucza-kino@gmail.com',
        'Beata Janowska',
        '08:30', '890800801', (SELECT id from cinema_address_dict
                                    where city='Warszawa' and zip_code = '01-340'));

INSERT INTO cinema(name, close_hour, email, manager, open_hour, phone, address_id)
VALUES ('Multikino Helios',
        '23:00',
        'helios@gmail.com',
        'Hieronim Hiacynt',
        '08:00', '600120120', (SELECT id from cinema_address_dict
                                    where city='Białystok' and zip_code = '11-540'));

INSERT INTO cinema(name, close_hour, email, manager, open_hour, phone, address_id)
VALUES ('Multikino Alfa',
        '00:00',
        'alfa@o2.pl',
        'Joanna Konieczna',
        '10:00', '640300456', (SELECT id from cinema_address_dict
                                    where city='Białystok' and zip_code = '22-300'));
