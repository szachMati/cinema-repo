
-- USER_ROLE_DICT
INSERT INTO user_role_dict(code) values ('ROLE_USER'), ('ROLE_ADMIN');

-- MOVIE_CATEGORY_DICT
INSERT INTO movie_category_dict(code) values
('COMEDY'),('THRILLER'),('HORROR'),('DRAMA'),('ACTION'),('CRIME'),('CARTOON'),
('ROMANTIC'), ('DOCUMENTARY'), ('FAMILY'), ('HISTORICAL'), ('SPORT'),
('SCIENCE_FICTION'), ('WESTERN'), ('WAR');

-- CINEMA_ADDRESS_DICT
INSERT INTO cinema_address_dict(city, street, street_number, zip_code) values
('Warszawa', 'Górczewska', '23', '00-280'),
('Warszawa', 'Al.Jerozolimskie', '102', '01-110'),
('Warszawa', 'Krucza', '11', '01-340'),
('Białystok', 'Warszawska', '1', '11-540'),
('Białystok', 'Zielona', '180', '22-300');


