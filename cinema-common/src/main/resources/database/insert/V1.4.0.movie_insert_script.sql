
--movie

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Czarownica 2', 2019, 7, 'NEW', 'ROMANTIC');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Joker', 2019, 9, 'NEW', 'THRILLER');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Hobbs & Shaw', 2019, 6, 'POPULAR', 'ACTION');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Fast and Furious 2', 2000, 9, 'CLASSIC', 'ACTION');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Przyjaciółki', 2005, 3, 'BAD', 'ROMANTIC');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Interstellar', 2015, 10, 'GOOD', 'SCIENCE_FICTION');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Psy 3', 2019, 7, 'POPULAR', 'CRIME');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'To 2', 2016, 5, 'AVERAGE', 'HORROR');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Piła 3', 1998, 9, 'CLASSIC', 'HORROR');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Bad Boys 4', 2020, 0, 'NEW', 'ACTION');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Lenny - cudowny pies', 2000, 6, 'AVERAGE', 'FAMILY');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'America Pie - Naked Mile', 1999, 7, 'CLASSIC', 'COMEDY');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'America Pie', 1996, 8, 'CLASSIC', 'COMEDY');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Informator', 1990, 2, 'BAD', 'DRAMA');

INSERT INTO MOVIE(create_time, name, production_year, rate, status, movie_category)
VALUES (current_timestamp, 'Zielona Mila', 2002, 10, 'NEUTRAL', 'DRAMA');
