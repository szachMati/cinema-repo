
--cinema_hall
INSERT INTO cinema_hall(name, cinema_id) values ('Gwiezdna', (Select id from cinema where name = 'Multikino Wola Park'));
INSERT INTO cinema_hall(name, cinema_id) values ('Kosmiczna', (Select id from cinema where name = 'Multikino Wola Park') );
INSERT INTO cinema_hall(name, cinema_id) values ('Atlantycka', (Select id from cinema where name = 'Multikino Wola Park')  );


INSERT INTO cinema_hall(name, cinema_id) values ('Złota', (Select id from cinema where name = 'Multikino Złote Tarasy'));
INSERT INTO cinema_hall(name, cinema_id) values ('Srebrna', (Select id from cinema where name = 'Multikino Złote Tarasy'));
INSERT INTO cinema_hall(name, cinema_id) values ('Czerwona', (Select id from cinema where name = 'Multikino Złote Tarasy'));

INSERT INTO cinema_hall(name, cinema_id) values ('Krótka', (Select id from cinema where name = 'Multikino Krucza Warszawa'));
INSERT INTO cinema_hall(name, cinema_id) values ('Szeroka', (Select id from cinema where name = 'Multikino Krucza Warszawa'));
INSERT INTO cinema_hall(name, cinema_id) values ('Duża', (Select id from cinema where name = 'Multikino Krucza Warszawa'));

INSERT INTO cinema_hall(name, cinema_id) values ('Boska', (Select id from cinema where name = 'Multikino Helios'));
INSERT INTO cinema_hall(name, cinema_id) values ('Grecka', (Select id from cinema where name = 'Multikino Helios'));

INSERT INTO cinema_hall(name, cinema_id) values ('Niebieska', (Select id from cinema where name = 'Multikino Alfa'));
INSERT INTO cinema_hall(name, cinema_id) values ('Żółta', (Select id from cinema where name = 'Multikino Alfa'));
