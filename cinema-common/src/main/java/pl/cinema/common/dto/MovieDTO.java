package pl.cinema.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.cinema.common.entity.MovieEntity;
import pl.cinema.common.enums.MovieCategory;
import pl.cinema.common.enums.MovieStatus;

import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MovieDTO {

    private Long id;
    private String name;
    private MovieStatus status;
    private Integer rate;
    private ZonedDateTime createTime;
    private ZonedDateTime updateTime;
    private Integer productionYear;
    private MovieCategory movieCategory;

    public MovieDTO(MovieEntity movieEntity) {
        this.id = movieEntity.getId();
        this.name = movieEntity.getName();
        if (movieEntity.getStatus() != null) {
            this.status = movieEntity.getStatus();
        }
        if (movieEntity.getRate() != null) {
            this.rate = movieEntity.getRate().getRateNumber();
        }
        this.createTime = movieEntity.getCreateTime();
        this.updateTime = movieEntity.getUpdateTime();
        this.productionYear = movieEntity.getProductionYear();
        if (movieEntity.getMovieCategory() != null) {
            this.movieCategory = MovieCategory.valueOf(movieEntity.getMovieCategory().getCode());
        }
    }


}
