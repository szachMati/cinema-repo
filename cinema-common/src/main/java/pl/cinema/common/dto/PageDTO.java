package pl.cinema.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class PageDTO<T> {

    private int totalElements;
    private int totalPages;
    private List<T> content;
}
