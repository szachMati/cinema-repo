package pl.cinema.common.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import pl.cinema.common.entity.dict.MovieCategoryDict;
import pl.cinema.common.enums.MovieRate;
import pl.cinema.common.enums.MovieStatus;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "movie")
public class MovieEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private MovieStatus status;

    @Column(name = "rate")
    @Enumerated(EnumType.ORDINAL)
    private MovieRate rate;

    @Column(name = "create_time")
    @CreationTimestamp
    private ZonedDateTime createTime;

    @Column(name = "update_time")
    @UpdateTimestamp
    private ZonedDateTime updateTime;

    @Column(name = "production_year")
    private Integer productionYear;

    @JoinColumn(name = "movie_category")
    @ManyToOne(fetch = FetchType.LAZY)
    private MovieCategoryDict movieCategory;

    @OneToMany(mappedBy = "movie", fetch = FetchType.LAZY)
    private List<SeanceEntity> seances;
}
