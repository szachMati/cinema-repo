package pl.cinema.common.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "seance")
public class SeanceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_time")
    private ZonedDateTime startTime;

    @Column(name = "end_time")
    private ZonedDateTime endTime;

    @JoinColumn(name = "cinema_hall_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CinemaHallEntity cinemaHall;

    @JoinColumn(name = "movie_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MovieEntity movie;

    @OneToMany(mappedBy = "seance", fetch = FetchType.LAZY)
    private List<BookingEntity> bookings;

}
