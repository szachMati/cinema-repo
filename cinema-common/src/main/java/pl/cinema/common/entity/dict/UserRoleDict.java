package pl.cinema.common.entity.dict;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "user_role_dict")
public class UserRoleDict extends DictionaryMappedSuperclass {
}
