package pl.cinema.common.entity;


import lombok.Data;
import pl.cinema.common.enums.SeatStatus;

import javax.persistence.*;

@Data
@Entity
@Table(name = "seat")
public class SeatEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //TODO do zamiany ew. na stringi
    @Column(name = "seat_row")
    private Integer seatRow;

    @Column(name = "seat_number")
    private Integer seatNumber;

    @Enumerated(EnumType.STRING)
    private SeatStatus status;

    @JoinColumn(name = "cinema_hall_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CinemaHallEntity cinemaHall;

    @JoinColumn(name = "booking_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private BookingEntity booking;


}
