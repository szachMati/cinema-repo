package pl.cinema.common.entity.dict;

import lombok.Data;

import javax.persistence.*;

@Data
@MappedSuperclass
public class DictionaryMappedSuperclass {
    @Id
    @Column(name = "code", unique = true, nullable = false)
    protected String code;
}
