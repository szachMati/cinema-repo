package pl.cinema.common.entity;


import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "[user]")
public class UserEntity extends UserMappedSuperclass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ColumnDefault("false")
    @Column(name = "enabled")
    private Boolean enabled;

    @ColumnDefault("false")
    @Column(name = "accepts_terms_of_service")
    private Boolean acceptsTermsOfService;

    @ColumnDefault("false")
    @Column(name = "accepts_newsletter")
    private Boolean acceptsNewsletter;

    @ColumnDefault("false")
    @Column(name = "accepts_push_notifications")
    private Boolean acceptsPushNotifications;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<UserRoleEntity> userRoles;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<BookingEntity> bookings;


}
