package pl.cinema.common.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "cinema_hall")
public class CinemaHallEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @JoinColumn(name = "cinema_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CinemaEntity cinema;

    @OneToMany(mappedBy = "cinemaHall", fetch = FetchType.LAZY)
    private List<SeanceEntity> seances;
}
