package pl.cinema.common.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.time.ZonedDateTime;

@Getter
@Setter
@MappedSuperclass
public class UserMappedSuperclass {

    @Column(name = "first_name")
    protected String firstName;

    @Column(name = "last_name")
    protected String lastName;

    @Email
    @Column(name = "email", nullable = false, unique = true)
    protected String email;

    @Pattern(regexp = "^[0-9]{9}")
    @Column(name = "phone", unique = true)
    protected String phone;

    @Column(name = "password")
    protected String password;

    @Column(name = "create_time")
    @CreationTimestamp
    private ZonedDateTime createTime;

    @Column(name = "update_time")
    @UpdateTimestamp
    private ZonedDateTime updateTime;
}
