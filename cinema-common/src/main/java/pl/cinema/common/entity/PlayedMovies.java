package pl.cinema.common.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Data
@Entity
@Table(name = "played_movies")
public class PlayedMovies {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_date")
    private ZonedDateTime startDate;

    @Column(name = "end_date")
    private ZonedDateTime endDate;

    @JoinColumn(name = "movie_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private MovieEntity movie;

    @JoinColumn(name = "cinema_id" , nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private CinemaEntity cinema;

}
