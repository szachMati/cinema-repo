package pl.cinema.common.entity.dict;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "movie_category_dict")
public class MovieCategoryDict extends DictionaryMappedSuperclass {
}
