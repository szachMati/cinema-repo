package pl.cinema.common.entity;


import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import pl.cinema.common.enums.BookingStatus;
import pl.cinema.common.enums.PaymentType;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "booking")
public class BookingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private BookingStatus bookingStatus;

    @CreationTimestamp
    @Column(name = "create_time")
    private ZonedDateTime createTime;

    @UpdateTimestamp
    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @Column(name = "expireTime")
    private ZonedDateTime expireTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_type")
    private PaymentType paymentType;

    @JoinColumn(name = "seance_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SeanceEntity seance;

    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    @OneToMany(mappedBy = "booking", fetch = FetchType.LAZY)
    private List<SeatEntity> seats;

}
