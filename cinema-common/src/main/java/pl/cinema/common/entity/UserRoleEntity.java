package pl.cinema.common.entity;

import lombok.Data;
import pl.cinema.common.entity.dict.UserRoleDict;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_role")
public class UserRoleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    @JoinColumn(name = "user_role")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserRoleDict userRole;
}
