package pl.cinema.common.entity;

import lombok.Data;
import pl.cinema.common.entity.dict.CinemaAddressDict;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.time.ZonedDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "cinema")
public class CinemaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String cinemaName;

    @Column(name = "phone")
    @Pattern(regexp = "^[0-9]{9}")
    private String phoneNumber;

    @Column(name = "open_hour")
    private String openHour;

    @Column(name = "close_hour")
    private String closeHour;

    @Column(name = "manager")
    private String managerName;

    @Column(name = "email")
    @Email
    private String email;

    @JoinColumn(name = "address_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CinemaAddressDict cinemaAddress;

    @OneToMany(mappedBy = "cinema", fetch = FetchType.LAZY)
    private List<PlayedMovies> movies;

    @OneToMany(mappedBy = "cinema", fetch = FetchType.LAZY)
    private List<CinemaHallEntity> halls;

}
