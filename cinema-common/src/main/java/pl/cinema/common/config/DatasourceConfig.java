package pl.cinema.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


@Configuration
public class DatasourceConfig {

    @Autowired
    private PostgresConfigurationParams postgresConfigurationParams;

    @Bean
    public DataSource dataSource() {
        return DataSourceBuilder.create()
                .driverClassName(postgresConfigurationParams.getDriverClassName())
                .url(postgresConfigurationParams.getUrl())
                .username(postgresConfigurationParams.getUserName())
                .password(postgresConfigurationParams.getPassword())
                .build();
    }




}
