package pl.cinema.common.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "db")
public class PostgresConfigurationParams {

    private String driverClassName;
    private String url;
    private String userName;
    private String password;

}
