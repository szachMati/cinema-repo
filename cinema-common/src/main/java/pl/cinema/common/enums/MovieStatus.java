package pl.cinema.common.enums;

public enum MovieStatus {
    NEW,
    POPULAR,
    CLASSIC,
    GOOD,
    BAD,
    AVERAGE,
    NEUTRAL

}
