package pl.cinema.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum MovieRate {

    ZERO(0),
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10);

    private final Integer rateNumber;

    public static MovieRate findRateByValue(Integer value){
        return Arrays.stream(values()).filter(rate -> rate.getRateNumber().equals(value))
                .findFirst()
                .orElse(null);
    }
}
