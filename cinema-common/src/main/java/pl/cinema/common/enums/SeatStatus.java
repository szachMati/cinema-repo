package pl.cinema.common.enums;

public enum SeatStatus {
    EMPTY, BUSY
}
