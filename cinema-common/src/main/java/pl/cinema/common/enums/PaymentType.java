package pl.cinema.common.enums;

public enum PaymentType {
    CARD,
    PHONE,
    BLIK,
    TRANSFER,
    CASH
}
