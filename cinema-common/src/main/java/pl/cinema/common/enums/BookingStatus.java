package pl.cinema.common.enums;

public enum BookingStatus {
    INPROGRESS,
    ACCEPTED,
    REJECTED
}
