package pl.cinema.common.enums;

public enum MovieCategory {
    COMEDY,
    THRILLER,
    HORROR,
    DRAMA,
    ACTION,
    CRIME,
    CARTOON,
    ROMANTIC,
    DOCUMENTARY,
    FAMILY,
    HISTORICAL,
    SPORT,
    SCIENCE_FICTION,
    WESTERN,
    WAR

}
