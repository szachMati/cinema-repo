package pl.cinema.common.utils;

import lombok.experimental.UtilityClass;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@UtilityClass
public class DateFormatter {

    public static final String UNIVERSAL_DATE_FORMAT = "yyyy-mm-dd hh:mm:ss";
    public static final String PLAYED_MOVIE_DATE_FORMAT = "yyyy-mm-dd hh:mm";

    public String formatDateToString(ZonedDateTime date, String pattern) {
        return date.format(DateTimeFormatter.ofPattern(pattern));
    }

    public ZonedDateTime formatStringToDate(String stringDate) {
        return ZonedDateTime.parse(stringDate);
    }
}
