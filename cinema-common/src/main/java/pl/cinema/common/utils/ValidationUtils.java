package pl.cinema.common.utils;


import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import pl.cinema.common.dto.MovieDTO;
import pl.cinema.common.enums.MovieCategory;
import pl.cinema.common.enums.MovieRate;
import pl.cinema.common.enums.MovieStatus;

import java.util.regex.Pattern;
import java.util.stream.Stream;

@Slf4j
@UtilityClass
public class ValidationUtils {

    public static final String MOVIE_PRODUCTION_YEAR_REGEXP = "^\\d[19|20]\\d{2}";

    public void validateMovie(MovieDTO movieDTO, boolean isEditing) {
        if (movieDTO.getName().trim().equals("")) {
            log.error("*** validateMovie() Could not found movie name");
            throw new IllegalArgumentException("Movie name cannot be empty!");
        }
        if (movieDTO.getProductionYear() == null) {
            log.error("*** validateMovie() Could not found movie production year");
            throw new IllegalArgumentException("Movie production year cannot be empty!");
        }
        if (!Pattern.matches(MOVIE_PRODUCTION_YEAR_REGEXP, movieDTO.getProductionYear().toString())) {
            log.error("*** validateMovie() Could not found movie production year");
            throw new IllegalArgumentException("Movie production year should be between 1900 and 2099!");
        }
        if (Stream.of(MovieStatus.values())
                .noneMatch(movieStatus -> movieStatus.equals(movieDTO.getStatus()))) {
            log.error("*** validateMovie() Could not found movie status");
            throw new IllegalArgumentException("Movie status is incorrect!");
        }
        if (Stream.of(MovieCategory.values())
                .noneMatch(category -> category.equals(movieDTO.getMovieCategory()))) {
            log.error("*** validateMovie() Could not found movie category");
            throw new IllegalArgumentException("Movie category is incorrect!");
        }

        if (isEditing) {
            if (movieDTO.getRate() == null) {
                log.error("*** validateMovie() Could not found movie rate");
                throw new IllegalArgumentException("Movie rate cannot be empty!");
            }
            if (Stream.of(MovieRate.values())
                    .noneMatch(movieRate -> !movieRate.equals(movieDTO.getRate()))) {
                log.error("*** validateMovie() Could not found movie rate");
                throw new IllegalArgumentException("Movie rate is incorrect!");
            }
        }
    }
}
