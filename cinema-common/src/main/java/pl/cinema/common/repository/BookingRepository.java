package pl.cinema.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cinema.common.entity.BookingEntity;

public interface BookingRepository extends JpaRepository<BookingEntity, Long> {
}
