package pl.cinema.common.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.cinema.common.entity.MovieEntity;
import pl.cinema.common.enums.MovieCategory;

public interface MovieRepository extends JpaRepository<MovieEntity, Long> {

    Page<MovieEntity> findByMovieCategoryOrderByRateDesc(MovieCategory category, Pageable pageable);

    MovieEntity findByName(String name);

}
