package pl.cinema.common.repository;

import pl.cinema.common.entity.dict.MovieCategoryDict;

public interface MovieCategoryRepository extends DictionaryRepository<MovieCategoryDict> {

    MovieCategoryDict findByCode(String code);
}
