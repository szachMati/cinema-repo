package pl.cinema.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cinema.common.entity.SeanceEntity;

public interface SeanceRepository extends JpaRepository<SeanceEntity, Long> {
}
