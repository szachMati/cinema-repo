package pl.cinema.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cinema.common.entity.CinemaEntity;

public interface CinemaRepository extends JpaRepository<CinemaEntity, Long> {

}
