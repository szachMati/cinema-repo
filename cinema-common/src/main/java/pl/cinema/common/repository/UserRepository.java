package pl.cinema.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cinema.common.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
}
