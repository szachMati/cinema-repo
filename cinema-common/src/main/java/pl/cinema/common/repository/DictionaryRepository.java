package pl.cinema.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import pl.cinema.common.entity.dict.DictionaryMappedSuperclass;

@NoRepositoryBean
public interface DictionaryRepository<T extends DictionaryMappedSuperclass> extends JpaRepository<T, String> {

    T findByCode(String code);
}
