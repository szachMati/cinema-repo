package pl.cinema.common.exception;


public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException() {
        super();
    }

    public ResourceNotFoundException(String cause) {
        super(cause);
    }
}
