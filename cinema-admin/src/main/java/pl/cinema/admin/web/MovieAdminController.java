package pl.cinema.admin.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.cinema.admin.service.MovieService;
import pl.cinema.common.dto.MovieDTO;

@RestController
@RequestMapping("/api/admin")
public class MovieAdminController {

    private MovieService movieService;

    @Autowired
    public MovieAdminController(MovieService movieService) {
        this.movieService = movieService;
    }

    @PostMapping("/movie")
    public ResponseEntity<MovieDTO> createMovie(@RequestBody MovieDTO movieDTO) {
        return new ResponseEntity<>(movieService.createMovie(movieDTO), HttpStatus.OK);
    }

    @GetMapping("/movie/{movieId}")
    public ResponseEntity<MovieDTO> getMovie(@PathVariable Long movieId) {
        return new ResponseEntity<>(movieService.getMovieById(movieId), HttpStatus.OK);
    }

    @DeleteMapping("/movie/delete/{movieId}")
    public ResponseEntity<?> deleteMovie(@PathVariable Long movieId) {
        movieService.deleteMovieById(movieId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/movie/edit")
    public ResponseEntity<MovieDTO> editMovie(@RequestBody MovieDTO movieDTO) {
        return new ResponseEntity<>(movieService.editMovie(movieDTO), HttpStatus.OK);
    }

}
