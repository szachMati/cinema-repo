package pl.cinema.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.cinema.common.CinemaCommon;
import pl.cinema.common.config.WebConfig;

@EnableJpaRepositories(basePackageClasses = {CinemaCommon.class})
@ComponentScan(basePackageClasses = {AdminApplication.class, CinemaCommon.class, WebConfig.class})
@EntityScan(basePackageClasses = {CinemaCommon.class})
@SpringBootApplication(scanBasePackageClasses = {AdminApplication.class, CinemaCommon.class})
@EnableConfigurationProperties
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
}
