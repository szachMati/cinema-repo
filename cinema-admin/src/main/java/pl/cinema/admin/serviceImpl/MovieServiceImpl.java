package pl.cinema.admin.serviceImpl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.cinema.admin.service.MovieService;
import pl.cinema.common.dto.MovieDTO;
import pl.cinema.common.dto.PageDTO;
import pl.cinema.common.dto.filter.MovieFilterDTO;
import pl.cinema.common.entity.MovieEntity;
import pl.cinema.common.enums.MovieRate;
import pl.cinema.common.exception.ResourceAlreadyExistsException;
import pl.cinema.common.exception.ResourceNotFoundException;
import pl.cinema.common.repository.MovieCategoryRepository;
import pl.cinema.common.repository.MovieRepository;
import pl.cinema.common.utils.ValidationUtils;
import java.util.Optional;

@Slf4j
@Service
public class MovieServiceImpl implements MovieService {

    private MovieRepository movieRepository;
    private MovieCategoryRepository movieCategoryRepository;

    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository,
                            MovieCategoryRepository movieCategoryRepository) {
        this.movieRepository = movieRepository;
        this.movieCategoryRepository = movieCategoryRepository;
    }

    @Override
    public MovieDTO createMovie(MovieDTO movieDTO) {
        log.info("*** createMovie() start creating movie");
        ValidationUtils.validateMovie(movieDTO, false);
        if (movieRepository.findByName(movieDTO.getName()) != null) {
            log.info("*** createMovie() creation process failed, movie {} was found", movieDTO.getName());
            throw new ResourceAlreadyExistsException("Movie with name: "+movieDTO.getName()+" already exists!");
        }
        MovieEntity movieEntity = mapToEntity(movieDTO);
        log.info("*** createMovie() end returning movie");
        return new MovieDTO(movieRepository.save(movieEntity));
    }

    private MovieEntity mapToEntity(MovieDTO movieDTO) {
        return MovieEntity.builder()
                .name(movieDTO.getName())
                .movieCategory(movieCategoryRepository.findByCode(movieDTO.getMovieCategory().name()))
                .productionYear(movieDTO.getProductionYear())
                .status(movieDTO.getStatus())
                .rate(MovieRate.findRateByValue(movieDTO.getRate()))
                .build();
    }

    @Override
    public MovieDTO getMovieById(Long movieId) {
        return new MovieDTO( movieRepository.findById(movieId)
                .orElseThrow(() -> {
                    log.error("*** getMovieById() cannot get movie with id {}", movieId);
                    return new ResourceNotFoundException("Movie with id= "+ movieId +" was not found");
                }));
    }

    @Override
    public MovieDTO editMovie(MovieDTO movieDTO) {
        log.info("*** editMovie() start editing movie");
        Optional<MovieEntity> optionalMovie = movieRepository.findById(movieDTO.getId());
        if (optionalMovie.isPresent()) {
            log.info("*** editMovie() with id: "+movieDTO.getId()+" was found");
            ValidationUtils.validateMovie(movieDTO, true);
            MovieEntity movieEntity = optionalMovie.get();
            if (!movieEntity.getName().equalsIgnoreCase(movieDTO.getName())) {
                movieEntity.setName(movieDTO.getName());
            }
            if (!movieEntity.getRate().getRateNumber().equals(movieDTO.getRate())) {
                movieEntity.setRate(MovieRate.findRateByValue(movieDTO.getRate()));
            }
            if (!movieEntity.getMovieCategory().getCode().equalsIgnoreCase(movieDTO.getMovieCategory().name())) {
                movieEntity.setMovieCategory(movieCategoryRepository.findByCode(movieDTO.getMovieCategory().name()));
            }
            if (!movieEntity.getProductionYear().equals(movieDTO.getProductionYear())) {
                movieEntity.setProductionYear(movieDTO.getProductionYear());
            }
            if (!movieEntity.getStatus().equals(movieDTO.getStatus())) {
                movieEntity.setStatus(movieDTO.getStatus());
            }
            log.info("*** editMovie() end returning movie");
            return new MovieDTO(movieRepository.save(movieEntity));

        } else {
            log.error("*** editMovie() could not found movie to update");
            throw new ResourceNotFoundException("Could not found movie "+movieDTO.getName());
        }
    }

    @Override
    public void deleteMovieById(Long id) {
        log.info("*** deleteMovieById() start");
        MovieEntity movieEntity = movieRepository.findById(id)
                .orElseThrow(() -> {
                    log.error("*** deleteMovieById() could not found movie with id {} ", id);
                    throw new ResourceNotFoundException("Cannot delete movie which not exists!");
                });

        log.info("*** deleteMovieById() movie with id: "+ id +" was found");
        movieRepository.deleteById(movieEntity.getId());
        log.info("*** deleteMovieById() end successfully deleted movie");
    }

    @Override
    public MovieDTO getMovieByName(String name) {
        MovieEntity movieEntity = movieRepository.findByName(name);
        if (movieEntity == null) {
            log.info("*** getMovieByName() movie with name {} doesnt exist", name);
            throw new ResourceNotFoundException("Movie "+name+" doesnt exist!");
        }
        return new MovieDTO(movieEntity);
    }

    // TODO
    @Override
    public PageDTO<MovieDTO> getFilteredMovies(MovieFilterDTO filter) {
        return null;
    }


}
