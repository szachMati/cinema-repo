package pl.cinema.admin.service;

import pl.cinema.common.dto.MovieDTO;
import pl.cinema.common.dto.PageDTO;
import pl.cinema.common.dto.filter.MovieFilterDTO;

public interface MovieService {

    MovieDTO createMovie(MovieDTO movieDTO);

    MovieDTO getMovieById(Long movieId);

    MovieDTO editMovie(MovieDTO movieDTO);

    void deleteMovieById(Long id);

    MovieDTO getMovieByName(String name);

    PageDTO<MovieDTO> getFilteredMovies(MovieFilterDTO filter);
}
