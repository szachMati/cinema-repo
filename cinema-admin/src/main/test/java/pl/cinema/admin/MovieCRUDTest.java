package pl.cinema.admin;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import pl.cinema.admin.service.MovieService;
import pl.cinema.common.dto.MovieDTO;
import pl.cinema.common.enums.MovieCategory;
import pl.cinema.common.enums.MovieRate;
import pl.cinema.common.enums.MovieStatus;
import pl.cinema.common.exception.ResourceNotFoundException;
import pl.cinema.common.repository.MovieRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**  class which is responsible for test CRUD operations from MovieService class
 *    and MovieRepository interface*/
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureWebMvc
@EnableWebMvc
public class MovieCRUDTest {

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private MovieDTO movieDTO;

    private HttpHeaders headers;

    private Long movieId;

    @Before
    public void init() {
        headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        movieDTO = MovieDTO.builder()
                .name("Bad Boys")
                .movieCategory(MovieCategory.CRIME)
                .productionYear(1996)
                .status(MovieStatus.CLASSIC)
                .rate(MovieRate.EIGHT.getRateNumber())
                .build();
    }

    @Test
    public void testMovieCreation() throws Exception {
        //TODO do doczytania
//        when(movieService.getMovieByName(movieDTO.getName())).thenThrow(new ResourceAlreadyExistsException("Already exists"));
        mockMvc.perform(post("/api/admin/movie")
                .headers(headers)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(movieDTO)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testGetMovieById() throws Exception {
        movieId = movieService.getMovieByName(movieDTO.getName()).getId();
        mockMvc.perform(get("/api/admin/movie/"+movieId)
                .headers(headers)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testEditMovieData() throws Exception {
        MovieDTO oldMovie = movieService.getMovieByName(movieDTO.getName());
        MovieDTO newMovie = oldMovie;
        newMovie.setProductionYear(2000);
        mockMvc.perform(put("/api/admin/movie/edit")
                    .headers(headers)
                    .content(objectMapper.writeValueAsString(newMovie)))
                    .andReturn();
    }


    @Test(expected = ResourceNotFoundException.class)
    public void testGetMovieByIdShouldThrownResourceNotFoundException() {
        Assert.assertNull("Test getting not existing movie" , movieService.getMovieById(20000L));
    }

    @Test
    public void testDeleteMovieById() {
        movieId = movieService.getMovieByName(movieDTO.getName()).getId();
        movieService.deleteMovieById(movieId);
        Assert.assertNull(movieRepository.findByName(movieDTO.getName()));
    }

}
