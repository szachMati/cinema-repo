import { Component, OnInit } from '@angular/core';
import {MovieService} from '../../services/movie.service';
import {MovieDTO} from '../../dto/movie-dto';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  movie: MovieDTO;

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
    this.movieService.getMovieById(1)
      .subscribe(response => {
        if (response) {
          console.log(response);
          this.movie = response;
        }
      },
        (error) => {
            console.log('error while getting movie');
        });
  }

}
