import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MovieDTO} from '../dto/movie-dto';
import {Utils} from '../utils/utils';

@Injectable({
  providedIn: 'root'
})
export class MovieService {


  constructor(private http: HttpClient) { }

  getMovieById(movieId: number): Observable<MovieDTO> {
    return this.http.get<MovieDTO>(`${Utils.API_ADMIN}/movie/${movieId}`);
  }


}
